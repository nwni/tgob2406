<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('principal');
})->name('inicio');

Route::prefix('quienes-somos')->group(function (){
    Route::get('integracion', function () {
        return view('quienes-somos.integracion');
    })->name('quienes.integracion');
    Route::get('objetivo-general', function () {
        return view('quienes-somos.objGeneral');
    })->name('quienes.objetivo.general');
});


Route::prefix('participacion-politica')->group(function (){
    Route::get('integracion-ayuntamientos', function () {
        return view('participacion-politica.integracionAyuntamientos');
    })->name('participacion.integracion.ayuntamientos');
    Route::get('integracion-legislatura', function () {
        return view('participacion-politica.integracionLegislatura');
    })->name('participacion.integracion.legislatura');
    
    Route::get('2016/candidaturas', function () {
        return view('participacion-politica.elecciones-2016.candidaturas');
    })->name('participacion.2016.candidaturas');
    Route::get('2016/estadisticas', function () {
        return view('participacion-politica.elecciones-2016.estadisticas');
    })->name('participacion.2016.estadisticas');
    Route::get('2016/resultados', function () {
        return view('participacion-politica.elecciones-2016.resultados');
    })->name('participacion.2016.resultados');
    
    Route::get('2018/campaña', function () {
        return view('participacion-politica.elecciones-2018.campaña');
    })->name('participacion.2018.campaña');
    Route::get('2018/cargos', function () {
        return view('participacion-politica.elecciones-2018.cargos');
    })->name('participacion.2018.cargos');
    Route::get('2018/cuestionario', function () {
        return view('participacion-politica.elecciones-2018.cuestionario');
    })->name('participacion.2018.cuestionario');
});


Route::prefix('marco-normativo')->group(function (){
    Route::get('marco-normativo', function () {
        return view('marco-normativo.marcoNormativo');
    })->name('marco.principal');
    Route::get('lineamientos', function () {
        return view('marco-normativo.lineamientos');
    })->name('marco.lineamientos');
    Route::get('manuales', function () {
        return view('marco-normativo.manuales');
    })->name('marco.manuales');
    Route::get('marco-internacional', function () {
        return view('marco-normativo.mjInternacional');
    })->name('marco.internacional');
    Route::get('marco-local', function () {
        return view('marco-normativo.mjLocal');
    })->name('marco.local');
    Route::get('marco-nacional', function () {
        return view('marco-normativo.mjNacional');
    })->name('marco.nacional');
    Route::get('reglamentos', function () {
        return view('marco-normativo.reglamentos');
    })->name('marco.reglamentos');
});


Route::prefix('actividades')->group(function (){
    Route::get('avances-mujeres', function () {
        return view('actividades.avancesppMujeres');
    })->name('actividades.avances');
    Route::get('violencia-politica', function () {
        return view('actividades.violenciaPolitica');
    })->name('actividades.violencia');
});


Route::get('publicaciones', function () {
    return view('publicaciones.publicaciones');
})->name('publicaciones');

// www.name.com/administrador/...
Route::prefix('administrador')->group(function (){
    
});
