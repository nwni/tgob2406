@extends('elementos.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <br>
            <br>
            <h2>Integración</h2><br>
            <p>
                Coordinar las acciones entre instituciones estatales, 
                organizaciones de la sociedad civil, academia y partidos políticos en favor 
                de la participación política y de la toma de decisiones públicas de las 
                mujeres en Colima, para lograr sinergias que cierren la brecha de género 
                en la materia, desde un enfoque de igualdad sustantiva entre mujeres y hombres, 
                al coordinar una política pública en atención a la responsabilidad del Estado Mexicano 
                en la materia.
            </p>
            <br><br>
            <h3>Este observatorio se conforma por:</h3>

            <ul>
                <li>Presidencia</li>
                    <p>
                        Mtra. Noemí Sofía Herrera Núñez, Titular de la Comisión de Equidad, Paridad y Perspectiva
                        de Género del Instituto Electoral del Estado de Colima.
                    </p>
                <li>Secretaría Técnica</li>
                    <p>
                        Dra. Mariana Martínez Flores, Directora del Instituto Colimense de las Mujeres.
                    </p>
                <li>Dirección Ejecutiva</li>
                    <p>
                        Lic. Guillermo de Jesús Navarrete Zamora, Magistrado Presidente del Tribunal Electoral del Estado
                        de Colima.
                    </p>
                <li>Integrantes Estratégicos</li>
                <li>Representantes de sociedad civil</li>
                    <ul>
                        <li>Instituto Griselda Álvarez, A.C.</li>
                        <li>100 por Colima, A.C.</li>
                        <li>Fundación IUS Género, A.C.</li>
                        <li>Asociación Colimense de Universitarias, A.C.</li>
                        <li>Proceder Derechos Humanos, A.C.</li>
                    </ul>
                <li>Invitadas académicas</li>
            </ul>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-3">
            @include('elementos.secciones')
        </div>        
    </div>
</div>
@endsection