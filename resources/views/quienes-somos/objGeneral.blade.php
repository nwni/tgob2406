@extends('elementos.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <br><br>
            <h2>Objetivo General</h2><br>
            <p>
                    Coordinar las acciones entre instituciones estatales, organizaciones de la 
                    sociedad civil, academia y partidos políticos en favor de la participación 
                    política y de la toma de decisiones públicas de las mujeres en Colima, 
                    para lograr sinergias que cierren la brecha de género en la materia, desde 
                    un enfoque de igualdad sustantiva entre mujeres y hombres, al coordinar una 
                    política pública en atención a la responsabilidad del Estado Mexicano en la materia.
            </p>

        </div>
        <div class="col-md-2"></div>
        <div class="col-md-3">
            @include('elementos.secciones')
        </div>        
    </div>
    <br><br>
</div>
@endsection