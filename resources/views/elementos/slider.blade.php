<div id="miSlider" class="carousel slide" data-ride="carousel">
    <!-- Slides -->
    <ol class="carousel-indicators">
      <li data-target="#miSlider" data-slide-to="0" class="active"></li>
      <li data-target="#miSlider" data-slide-to="1"></li>
      <li data-target="#miSlider" data-slide-to="2"></li>
      <li data-target="#miSlider" data-slide-to="3"></li>
    </ol>

    <!-- Imagenes del slider -->
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('images/slider/slider_01.jpg') }}" alt="Los Angeles" style="width:100%;">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/slider/slider_02.jpg') }}" alt="Chicago" style="width:100%;">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/slider/slider_03.jpg') }}" alt="New york" style="width:100%;">
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/slider/slider_04.jpg') }}" alt="New york" style="width:100%;">
        </div>
    </div>

    <!-- Controles -->
    <a class="carousel-control-prev" href="#miSlider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#miSlider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Siguiente</span>
    </a>
</div>