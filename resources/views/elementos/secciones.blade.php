<div class="secciones">
    <ul class="nav flex-column nav-secciones">
        <li class="nav-item">
            <a class="nav-link btn-seccion" href="{{ route('inicio') }}">Inicio</a>
        </li>
        <li class="nav-item">
            <a class="nav-link btn-seccion" data-toggle="collapse" href="#collapseQuienesSomos" role="button" aria-expanded="false" aria-controls="collapseQuienesSomos">Quiénes somos<span class="fa fa-chevron-down icon-seccion" aria-hidden="true"></span></a>
            <div class="collapse" id="collapseQuienesSomos">
                <a class="nav-link collapse-seccion-text" href="{{ route('quienes.integracion') }}">Integración</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('quienes.objetivo.general') }}">Objetivo General</a>
                <a class="nav-link collapse-seccion-text" href="#">Glosario</a>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link btn-seccion" data-toggle="collapse" href="#collapseParticipacionPolitica" role="button" aria-expanded="false" aria-controls="collapseParticipacionPolitica">Participación política<span class="fa fa-chevron-down icon-seccion" aria-hidden="true"></span></a>
            <div class="collapse" id="collapseParticipacionPolitica">
                <a class="nav-link collapse-seccion-text" href="{{ route('participacion.integracion.ayuntamientos') }}">Integración de los 58 ayuntamientos</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('participacion.integracion.legislatura') }}">Integración de la LXII legislatura</a>

                <a class="nav-link btn-seccion seccion-2nd-lvl" data-toggle="collapse" href="#collapseParticipacionPolitica2016" role="button" aria-expanded="false" aria-controls="collapseParticipacionPolitica2016">Elecciones 2016<span class="fa fa-chevron-down icon-seccion" aria-hidden="true"></span></a>
                <div class="collapse" id="collapseParticipacionPolitica2016">
                    <a class="nav-link collapse-seccion-text" href="{{ route('participacion.2016.candidaturas') }}">Candidaturas</a>
                    <a class="nav-link collapse-seccion-text" href="{{ route('participacion.2016.resultados') }}">Resultados</a>
                    <a class="nav-link collapse-seccion-text" href="{{ route('participacion.2016.estadisticas') }}">Estadísticas</a>
                </div>    
                <a class="nav-link btn-seccion seccion-2nd-lvl" data-toggle="collapse" href="#collapseParticipacionPolitica2018" role="button" aria-expanded="false" aria-controls="collapseParticipacionPolitica2018">Elecciones 2018<span class="fa fa-chevron-down icon-seccion" aria-hidden="true"></span></a>
                <div class="collapse" id="collapseParticipacionPolitica2018">
                    <a class="nav-link collapse-seccion-text" href="{{ route('participacion.2018.cargos') }}">Cargos de elección</a>
                    <a class="nav-link collapse-seccion-text" href="{{ route('participacion.2018.cuestionario') }}">Cuestionario</a>
                    <a class="nav-link collapse-seccion-text" href="{{ route('participacion.2018.campaña') }}">Campaña</a>                
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link btn-seccion" data-toggle="collapse" href="#collapseMarcoNormativo" role="button" aria-expanded="false" aria-controls="collapseMarcoNormativo">Marco normativo<span class="fa fa-chevron-down icon-seccion" aria-hidden="true"></span></a>
            <div class="collapse" id="collapseMarcoNormativo">
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.principal') }}">Marco Normativo</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.internacional') }}">Marco Jurídico Internacional</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.nacional') }}">Marco Jurídico Nacional</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.local') }}">Marco Jurídico Local</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.reglamentos') }}">Reglamentos</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.lineamientos') }}">Lineamientos</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('marco.manuales') }}">Manuales</a>
            </div>            
        </li>
        <li class="nav-item">
            <a class="nav-link btn-seccion" data-toggle="collapse" href="#collapseActividades" role="button" aria-expanded="false" aria-controls="collapseActividades">Actividades<span class="fa fa-chevron-down icon-seccion" aria-hidden="true"></span></a>
            <div class="collapse" id="collapseActividades">
                <a class="nav-link collapse-seccion-text" href="{{ route('actividades.avances') }}">Avances en la participación política de las Mujeres</a>
                <a class="nav-link collapse-seccion-text" href="{{ route('actividades.violencia') }}">Violencia Política</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link btn-seccion" href="{{ route('publicaciones') }}">Publicaciones</a>
        </li>        
    </ul>
</div>