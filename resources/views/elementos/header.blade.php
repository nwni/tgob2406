<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-header fixed-top">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse move-right" id="navbarNavAltMarkup">
        <div class="col-md-3">
            <a class="navbar-brand" href="{{ route('inicio') }}">
                <img src="{{ asset('images/header/logo.png') }}" class="logo-header" alt="logo">
            </a>
        </div>
        <div class="col-md-9">
            <div class="navbar-nav nav-links">
                <a class="nav-item nav-link" href="{{ route('inicio') }}">Inicio</a>

                <div class="dropdown">
                    <a class="nav-item nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quiénes somos</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('quienes.integracion') }}">Integración</a>
                        <a class="dropdown-item" href="{{ route('quienes.objetivo.general') }}">Objetivo General</a>
                        <a class="dropdown-item" href="#">Glosario</a>
                    </div>
                </div>  

                <div class="dropdown">
                    <a class="nav-item nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Participación Política</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('participacion.integracion.ayuntamientos') }}">Integración de los 58 ayuntamientos</a>
                        <a class="dropdown-item" href="{{ route('participacion.integracion.legislatura') }}">Integración de la LXII legislatura</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Elecciones 2016</a>
                            <a class="dropdown-item" href="{{ route('participacion.2016.candidaturas') }}">Candidaturas</a>
                            <a class="dropdown-item" href="{{ route('participacion.2016.resultados') }}">Resultados</a>
                            <a class="dropdown-item" href="{{ route('participacion.2016.estadisticas') }}">Estadísticas</a>
                            <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Elecciones 2018</a>
                            <a class="dropdown-item" href="{{ route('participacion.2018.cargos') }}">Cargos de elección</a>
                            <a class="dropdown-item" href="{{ route('participacion.2018.cuestionario') }}">Cuestionario</a>
                            <a class="dropdown-item" href="{{ route('participacion.2018.campaña') }}">Campaña</a>
                    </div>                    
                </div>

                <div class="dropdown">
                    <a class="nav-item nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Marco Normativo</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('marco.principal') }}">Marco Normativo</a>
                        <a class="dropdown-item" href="{{ route('marco.internacional') }}">Marco Jurídico Internacional</a>
                        <a class="dropdown-item" href="{{ route('marco.nacional') }}">Marco Jurídico Nacional</a>
                        <a class="dropdown-item" href="{{ route('marco.local') }}">Marco Jurídico Local</a>
                        <a class="dropdown-item" href="{{ route('marco.reglamentos') }}">Reglamentos</a>
                        <a class="dropdown-item" href="{{ route('marco.lineamientos') }}">Lineamientos</a>
                        <a class="dropdown-item" href="{{ route('marco.manuales') }}">Manuales</a>
                    </div>
                </div>

                <div class="dropdown">
                    <a class="nav-item nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actividades</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('actividades.avances') }}">Avances en la participación política de las Mujeres</a>
                        <a class="dropdown-item" href="{{ route('actividades.violencia') }}">Violencia Política</a>
                    </div>
                </div>

                <a class="nav-item nav-link" href="{{ route('publicaciones') }}">Publicaciones</a>
            </div>
        </div>
    </div>
</nav>